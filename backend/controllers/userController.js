const User = require('../models/User');

exports.getUsers = function(req, res){
    User.find()
        .then((result) => {
            res.status(200).send(result);
        })
        .catch((err) => {
            res.status(500).send(err);
        });
}

exports.editUser = function(req, res){
    const user = {
        _id: req.body._id,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        deliveryAddress: req.body.deliveryAddress,
        billingAddress: req.body.billingAddress
    }
    User.findOneAndUpdate({_id: user._id}, user)
        .then((response) => {
            res.status(200).send(response);
        })
        .catch(err => {
            console.log(err);
        });
}

//For adding sample data to database
exports.addUser = function(req, res){
    const user = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        deliveryAddress: req.body.deliveryAddress, 
        billingAddress: req.body.billingAddress
    });
    user.save()
        .then(result => {
            res.status(200).send(result);
        })
        .catch(err => res.status(500).send(err));
}