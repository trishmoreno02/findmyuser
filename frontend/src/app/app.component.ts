import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { User } from './app.interface';
import { AppService } from './app.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  displayedColumns: string[] = ['id', 'firstName', 'lastName', 'deliveryAddress', 'billingAddress', 'edit'];
  dataSource: User[];
  editId: number;
  userForm: FormGroup = new FormGroup({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    deliveryAddress: new FormControl('', Validators.required),
    billingAddress: new FormControl('', Validators.required),
  });

  constructor(private service: AppService, private _snackBar: MatSnackBar){}

  ngOnInit(){
    this.service.getUsers().subscribe((res: User[]) => {
      this.dataSource = res;
    });
  }

  editRow(user: User){
    this.userForm.get('firstName').patchValue(user.firstName);
    this.userForm.get('lastName').patchValue(user.lastName);
    this.userForm.get('deliveryAddress').patchValue(user.deliveryAddress);
    this.userForm.get('billingAddress').patchValue(user.billingAddress);
    this.editId = user._id;
  }

  saveRow(user: User){
    if(this.userForm.valid)
    {
      this.editId = 0;
      user.firstName = this.userForm.get('firstName').value;
      user.lastName = this.userForm.get('lastName').value;
      user.deliveryAddress = this.userForm.get('deliveryAddress').value;
      user.billingAddress = this.userForm.get('billingAddress').value;
      this.service.editUser(user).subscribe((_) => {
        this._snackBar.open('Saved!', null, { duration: 2000 })
        this.userForm.reset();
      }, (err) => {
        this._snackBar.open('An error has occurred.', null, { duration: 2000 });
        this.userForm.reset();
      });
    }
  }
}
