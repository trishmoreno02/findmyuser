# FindMyUser

## Requirements
1. Node.js 12.x.x
2. npm 6.x.x

## Backend Setup
1. Go to `backend` folder, and hit `npm install` in the terminal
2. Create .env file and copy the contents of .env.example. For the purposes of this coding exam, the values are: <br>
DB_USER=admin <br>
DB_PASSWORD=admin123 <br>
DB_HOST=cluster0.b00ql.mongodb.net <br>
DB_NAME=findmyuser <br>
PORT=8081 <br>
3. To run, hit `npm run start` in the terminal and it should run in localhost.

## Frontend Setup
1. Go to `frontend` folder, and hit `npm install` in the terminal
2. To compile and run, hit `npm run start` in the terminal and it should be available in localhost port 4200