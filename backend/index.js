const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const bodyParser = require('body-parser');

const userController = require('./controllers/userController');

require('dotenv').config();

const dbUsername = process.env.DB_USER;
const dbPassword = process.env.DB_PASSWORD;
const dbHost = process.env.DB_HOST;
const dbName = process.env.DB_NAME;
const port = process.env.PORT;

const app = express();

app.use(cors());
app.use(bodyParser.json());

const mongoDBURI = `mongodb+srv://${dbUsername}:${dbPassword}@${dbHost}/${dbName}?retryWrites=true&w=majority`;
mongoose.connect(mongoDBURI, { useNewUrlParser: true, useUnifiedTopology: true })
        .then(res => {
            serverInit();
        })
        .catch(err => console.log('MongoDB connection error: ', err));


//ROUTES
app.get('/users', userController.getUsers);
app.post('/editUser', userController.editUser);
app.post('/addUser', userController.addUser);

function serverInit(){
    const server = app.listen(port);
    const host = server.address().address;
    console.log(`Server started at address ${host} port ${port}`);
}